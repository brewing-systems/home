
=============
Customization
=============

------------------
Weldless bulkheads
------------------

An easier option to brazing tri-clamp fittings onto the tanks is to install
weldless bulkheads. For this, you'll need standard q.max punches.

* 41mm for 1.5" tri-clamp fittings.
* 47mm for 2" tri-clamp fittings.

You can buy the fittings as a complete set with gaskets. Ensure that the ID
for 1.5" is 38mm and for 2" is 51mm.

Ensure there are no leaks before using the system.

--------------
Pump whirlpool
--------------

Use a pump and two dip tubes (https://bit.ly/3rfLJQF) to force a whirlpool in
the HLT.

In addition to the standard part list, you'll require:

* A suction pump with 1.5" 19mm ID tri-clamp fittings. Some pumps require you
  to attach a 19mm ID hose to the pump, then 19mm barb to tri-clamp fittings.
  I've used the *Novax 20b* with good success.
* One extra valve. Otherwise, do not attach a valve to the boil kettle and
  transfer the cooled wort via a siphon.

Attach the dip tubes to two of 1.5" bulkheads on the HLT. Position them
in opposite directions so the suction flow is the same as the input flow.

------------
Pump cooling
------------

.. note::
    Attach a dip tube in place of the thermowell in this instance and use an
    external thermometer such as an IR thermometer to gauge the temperature.

Use a counter-flow chiller and a pump in order to cool your wort externally.
You can also position the dip tubes and make a whirlpool in the boil kettle.

In addition to the standard part list, you'll require a suction pump with 1.5"
19mm ID tri-clamp fittings. Some pumps require you to attach a 19mm ID hose to
the pump, then 19mm barb to tri-clamp fittings. I've used the *Novax 20b* with
good success.

----
RIMS
----

.. note::
  Use either a 1.5" or 2" RIMS as needed.

* A fitted false bottom to provide maximum filtration surface area and flow
  fate.
* A RIMS setup such as https://bit.ly/2MT7wyE.
* A basic gravity fed magnetic pump with 1.5" 19mm ID tri-clamp fittings. Most
  likely the pump will have 1/2" BSP male connections - compatible with 1/2"
  BSP female thread to 1.5" 19mm ID tri-clamp.
* A double-side weldless tri-clamp bulkhead such as https://bit.ly/3axx7W2.
* A tee connection such as https://bit.ly/39G1nz2. Attach this on the inside
  of the double-side weldless tri-clamp bulkhead.
* A PT100 thermometer to tri-clamp such as https://bit.ly/36ERG1X. Ensure the
  length fits in your tee connection. Attach the same 3-wire data/sensor male
  plug as in the :doc:`/docs/parts-list`.
* A method to sparge the wort back onto the mash bed. In the past, I used a
  spiral cone spray nozzle such as https://bit.ly/3tnIj03 and a angled
  tri-clamp fitting to aim it at the mash.

Install the double-sided weldless bulkhead at the top of you mash tun.
Attach the pump inlet via tubing from the outlet of the mash tun down to the
floor or at an appropriate height to prime it. Attach the RIMS outlet directly
to the mash inlet. Attach the pump outlet to the RIMS inlet. Place the tee and
thermometer between the inside of the double-sided weldless tri-clamp and your
sparge setup.

-----
Power
-----

Adjust the power as needed. Use the maximum available in your house - you can
adjust the power using your PID controller.

This set-up is compatible with 3-phase 240v power supplies with the following
notes:

* Ensure the 3-phase elements have the same tri-clamp fittings as installed on
  your tanks.
* Replace the single phase SSR in your control box with a 3-phase SSR and
  heatsink. These will be much larger than their standard 1-phase equivalents.
* Replace the main switch, power, and element socket with 3-phase equivalents.
* Ensure you wire the elements correctly - as either delta or y connections.
