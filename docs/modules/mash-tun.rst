-------------
40 L mash tun
-------------

This mash tun steeps the grain at a consistent temperature for a limited amount
of time. It relies on insulation to hold temperature, and a false bottom to
filter the wort following the mash.

They can be difficult to source, but do not require any special valve.
The referenced mash tun has a bottom outlet to a roughly 12 mm tap with
a washer and a 1/4" BSP nut fixing the tap on the inside of the tank. In this
case, replace the interior fixing nut with a 1/4" BSP to 8 mm barb connection.

Failing this, use a RIMS system as described in :doc:`/docs/customization`.
This is more complicated and expensive.

To transfer wort to the boiler, use gravity.

**Parts**

* 1x 40L insulated mash
* 1x Domed false bottom with 3/8" / 9.5 mm barb
* 1x 1/4" BSP to barb
* 10cm 9.5mm ID silicon tube
* 1.5m 1/2" / 12mm ID silicon tube

**Assembly**
