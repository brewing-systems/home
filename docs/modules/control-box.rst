-----------
Control box
-----------

* 1x Junction box
* 1x PID panel mount controller
* 1x > 20 a single phase SSR
* 1x W shaped heat sink
* 1x Single phase 0-1 rotary panel mount switch
* 1x 3 wire data/sensor female panel mount socket
* 1x Female panel mount power socket
* 1x Male panel mount power socket
* 1x RCD05 inline RCD
* 3m single phase cable to female plug and T12 RCD male plug
* 1x PT100 probe
