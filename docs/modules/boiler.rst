-----------
33 L Boiler
-----------

This boiler boils the wort following the mash. If desired, it holds any
temperature for hop steeping. It relies on an electric element and thermocouple
connected to the control box. Following the boil and cooling, use
a siphon to remove the wort.

**Parts**

* 1x 33L stainless steel tank
* 2x 1.5" TC bulkhead
* 1x 2" TC bulkhead
* 1x 1.5" TC butterfly valve
* 1x 1.5" TC to 19mm hose barb
* 1x power element with 3m cable to male power plug

**Assembly**

* One 1.5" TC double-sided bulkhead as a main valve. Add at a height that will
  allow a butterfly valve enough space on the bottom when the tank sits flat.
* One 1.5" TC bulkhead at 1/3 of the total height for the thermometer. Ensure
  that you position it so it does not impede operation of the main valve.
* One 2" TC bulkhead at the opposing side to the main valve. At a height that
  will allow the TC clamp enough space at the bottom when the tanks sits flat.
