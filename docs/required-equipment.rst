
==================
Required equipment
==================

* Electric drill
* Hex wrench set
* Pipe wrench
* 1.5" Q.Max punch and flare

	https://ebay.to/2L2Psli

* 2" Q.Max punch and flare

	https://ebay.to/2L2Psli

* Brazing equipment
* Step drill bit 8,10,24,30mm
* Drill bit 4,5mm
* Cable stripper
* Cable crimp for 1.5mm wire
* Screw driver
