import subprocess # allows us to pull in the latext git commit for the displayed content using the commit_id_develop tag below, from https://stackoverflow.com/questions/61579937/how-to-access-the-git-commit-id-in-sphinxs-conf-py. List most recent tag via `git describe --tags --abbrev=0` using commit_id_develop as example
import sphinx_rtd_theme

project = u'Home-Brewery' # Unique name for the document, mirrors the gitalb_repo name. Update in *_static/latex/mystle.sty* also.
master_doc = 'index' # Relative to sphinx-build location. Keep above *latex_documents* Rela
author = u'David Image' # Proprietary and confidential.
copyright = author # if different to author, otherwise put author
commit_id = commit_id = subprocess.check_output(['git', 'rev-parse', '--short=8', 'HEAD']).strip().decode('ascii')
commit_date = subprocess.check_output(['git', 'log', '-1', '--date=iso', '--format=%ad', 'HEAD']).strip().decode('ascii')
#most_recent_tag = subprocess.check_output(['git', 'describe', '--tags', '--abbrev=0']).strip().decode('ascii')
#most_recent_tag = subprocess.check_output(['git', 'describe', '--tags', '--abbrev=0']).strip().decode('ascii')
version = commit_id

# these values appear in the document
rst_prolog = """
.. |commit_id| replace:: {commit_id_prolog}
.. |commit_date| replace:: {commit_date_prolog}
.. |legal| replace:: {legal_prolog}
""".format(commit_id_prolog = commit_id, commit_date_prolog = commit_date,legal_prolog = copyright) # this format function allows us to pass variables into the prolog. Use it in the same way for the epilog

# Add below to rst_prolog for full functionality
#.. |commit_id| replace:: {commit_id_prolog}
#.. |commit_date| replace:: {commit_date_prolog}
#.. |most_recent_tag| replace:: {most_recent_tag_prolog}
# commit_id_prolog = commit_id, commit_date_prolog = commit_date, most_recent_tag_prolog = most_recent_tag

html_context = { # these settings are for the html template at source/_templates/layout.html
    "display_gitlab": True, # Integrate Gitlab
    "gitlab_user": "-/ide/project/brewing-systems", # Username
    "gitlab_repo": "home", # Repo name
    "gitlab_version": "master", # Version
    "conf_py_path": "/-/", # Path in the checkout to the docs root
    "commit_id": commit_id,
    "commit_date": commit_date,
    "theme_vcs_pageview_mode": "edit",
    #"most_recent_tag": most_recent_tag,
}

latex_engine = 'xelatex'
latex_elements = {
    'papersize': 'a4paper', # a4paper/letterpaper
    'pointsize': '11pt', # Change linespacing via source/_static/latex/mystle.sty or for the title spage via maketitle preamble below
    'figure_align': 'H',
    'sphinxsetup':'hmargin={2cm}, vmargin={2.5cm}, verbatimwithframe=true, TitleColor={rgb}{0,0,0}, OuterLinkColor={rgb}{0.208,0.374,0.486}, shadowsize=2pt, dangerborder=0.5pt, warningborder=0.5pt, cautionborder=0.5pt, attentionborder=0.5pt,',
    'printindex':"",
    'preamble': r'\usepackage{base}', # Adjust latex preamble in this file, ensure you set the correct document properties within
    # Use maketitle below to change title page and header/footer. This inserts content AFTER the document start tag in latex
    'maketitle': r'''

        %%% titlepage %%%
        \makeatletter
        \begin{titlepage}
        \newgeometry{left=6cm}
        \newcommand{\colorRule}[3][black]{\textcolor[HTML]{ 5fb55d }{\rule{#2}{#3}}} % Adjust line colour here
        \begin{flushleft}
        \noindent
        \\[-1em]
        \color[HTML]{5F5F5F}
        \makebox[0pt][l]{\colorRule[ 5fb55d ]{1.3\textwidth}{4pt}}  % Adjust line colour here
        \par
        \noindent

        {
        \vfill
        \noindent \begin{spacing}{2}{\huge \textbf{\textsf{\@title}}}
        \vskip 1em
        %{\Large \textsf{Description}} % Add description here
        %\vskip 1em
        \noindent
        {\Large \textsf{\version}}
        \end{spacing}
        \vfill
        }

        \noindent
        %\includegraphics[width=200pt]{../../_static/images/Kandou-logo-RGB.png}
        %\vskip 1em
        \textsf{\@title-\version} % Add doc reference information here
        \vskip 1em
        \textsf{\textcopyright \@author}

        \end{flushleft}
        \end{titlepage}
        \makeatother
        \restoregeometry

        %%% End titlepage %%%

        %%% Header and footer
        \makeatletter
        \fancypagestyle{plain}{
         \fancyhf{}
         \fancyhead[L]{\scriptsize \textsf{\@title}}
         %\fancyhead[R]{\scriptsize \textsf{\href{ online-url }{Online version}}} % address to document homepage on CMS
         \fancyfoot[R]{\scriptsize \textsf{\@author}}
         \fancyfoot[L]{\scriptsize \textsf{\@title-\version}}
         \fancyfoot[C]{\scriptsize \textsf{Page \thepage}}
         \renewcommand{\headrulewidth}{0.4pt}
         \renewcommand{\footrulewidth}{0.4pt}
        }

        \fancypagestyle{normal}{
         \fancyhf{}
         \fancyhead[L]{\scriptsize \textsf{\@title}}
         %\fancyhead[R]{\scriptsize \textsf{\href{ online-url }{Online version}}} % address to document homepage on CMS
         \fancyfoot[R]{\scriptsize \textsf{\@author}}
         \fancyfoot[L]{\scriptsize \textsf{\@title-\version}}
         \fancyfoot[C]{\scriptsize \textsf{Page \thepage}}
         \renewcommand{\headrulewidth}{0.4pt}
         \renewcommand{\footrulewidth}{0.4pt}
        }
        \makeatother
        %%% End header and footer
    ''',
}

# Manually update *project*-*version* and *project* according to above variables
latex_documents = [
    (master_doc, project+'.tex', project, author, 'howto'),
]

# -- General configuration ---------------------------------------------------
release = version
numfig = True
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.autosectionlabel',
    'sphinx_rtd_theme',
    ]

# The below epilog adds a line break function for the tables. Use via |br| in your table
rst_epilog = """
.. role:: raw-html(raw)
   :format: html

.. role:: raw-latex(raw)
   :format: latex

.. |br| replace:: :raw-html:`<br>`:raw-latex:`\linebreak`
"""

autosectionlabel_prefix_document = True
templates_path = ['_templates']
source_suffix = {
    '.rst': 'restructuredtext',
}
language = None
exclude_patterns = ['_build', '_drafts']
pygments_style = None
html_theme = "sphinx_rtd_theme"

def setup(app):
    app.add_css_file('base.css') # custom overrides for RTD theme

html_show_sourcelink = False
html_static_path = ['_static']
latex_additional_files = ['_static/base.sty']
latex_show_pagerefs = True
latex_show_urls = 'inline'
intersphinx_mapping = {'https://docs.python.org/': None}
todo_include_todos = True
todo_emit_warnings = True
latex_use_xindy = False # Xindy breaks the gitlab CI, could also try adding `apt install xindy` to gitlab-ci.yml
