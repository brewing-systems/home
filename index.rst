
.. raw:: latex

  \clearpage

------------
Home brewery
------------

.. note::
  * This is a work in progress
  * Add images

Scope
	\

	This is a checklist to build a 33 L 240V 3.5kW electric gravity based
	brewing system. Adjust any of the six modules in order to customize it.

	1. 50 L hot tank
	2. 40 L insulated mash tun
	3. 33 L boiler
	4. Cooler
	5. Control box
	6. Accessories

	See each module below for a basic assembly guide and tips. See the
	:doc:`/docs/customization` section for more information on how to adjust the
	basic setup to acheive different results.

	All links are examples only. For any further help, contact david@egami.ch

Warnings
	\

	.. warning::
	  Do not attempt unless you are competent with electrics. This checklist
	  assumes a working knowledge of connecting electrical wires and equipment.

	.. warning::
	  You are responsible for ensuring the safe assembly and operation of this
	  brewing system.

.. toctree::
  :maxdepth: 3
  :hidden:

  docs/required-equipment
  docs/parts-list
  docs/modules
  docs/customization

:ref:`genindex`
